import sys
import os.path
from shutil import copy
y, yes = 'y', 'yes'
n, no = 'n', 'no'

if len(sys.argv) > 2:
    print('Too many arguments')
elif len(sys.argv) < 2:
    print('Not enough arguments')
else:
    try:
        if os.path.isfile(sys.argv[1]):
            error = 0
            print('Start of excecution')
            with open(os.path.join(sys.argv[1]) as config:
                for line in config:
                    if line.strip() == '<file':
                        source_path = next(config).strip().split('=')
                        source_path = source_path[1].strip('"')
                        dest_path = next(config).strip().split('=')
                        dest_path = dest_path[1].strip('"')
                        file_name = next(config).strip().split('=')
                        file_name = file_name[1].strip('"')

                        file_path = os.path.join(source_path, file_name)
                        
                        if os.path.isdir(dest_path):
                            if os.path.isfile(file_path):
                                if os.path.isfile(os.path.join(dest_path, file_name)):
                                    a_corr = False
                                    while a_corr == False:
                                        try:
                                            answer = input('A file with the same name ({}) already exists and will be overwritten, y/n?\n'.format(file_name))
                                            if answer.lower() in ['y', 'yes']:
                                                copy(file_path, dest_path)
                                                a_corr = True
                                            elif answer.lower() in ['n', 'no']:
                                                a_corr = True
                                        except:
                                            a_corr = False
                                else:
                                    copy(file_path, dest_path)
                            else:
                                print('There is no such file: {}'.format(file_path))
                                error += 1
                        else:
                            print('There is no such directory: {}'.format(dest_path))
                            error += 1

            print('Total number of errors: {}'.format(error))
        else:
            print('No such config file: {}'.format(sys.argv[1]))
    except PermissionError:
        print('Permission denied')
    finally:
        print('Excecution is completed')
